use toy_rsa::*;

#[test]
fn encrypt_decrypt_works() {
    let msg1:u32 = 123456;
    let (p,q) = genkey();
    let pub_key = p as u64 * q as u64;
    let en_msg = encrypt(pub_key, msg1);
    let dec_text = decrypt((p,q), en_msg);
    assert_eq!(msg1, dec_text);
}

